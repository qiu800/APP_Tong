package com.sum.tong;

import android.view.View;

/**
 * Created by Sen on 2018/2/28.
 */

public interface AdapterItemClickListener<T> {
    void onItemClickListener(T data, int position, View view);
}
