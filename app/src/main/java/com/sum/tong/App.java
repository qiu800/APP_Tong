package com.sum.tong;

import android.app.Application;

import cn.bmob.v3.Bmob;

/**
 * Created by Sen on 2018/2/24.
 */

public class App extends Application {

    public static App mInstance;

    public Cache cache;

    public static App getInstance(){
        if(mInstance == null){
            synchronized(App.class){
                if(mInstance == null)
                    mInstance = new App();
            }
        }
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Bmob.initialize(this, "69df625ea3c10d039054db18cf5a33ec");
    }

    public Cache getCache(){
        if(cache == null){
            cache = new Cache();
        }
        return cache;
    }
}
