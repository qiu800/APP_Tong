package com.sum.tong;

import com.sum.tong.data.ServiceCategory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sen on 2018/3/23.
 */

public class Cache {

    private List<ServiceCategory> serviceCategoryCacheList;
    private Map<String, ServiceCategory> serviceCategoryMap;

    public List<ServiceCategory> getServiceCategoryCacheList() {
        return serviceCategoryCacheList;
    }

    public void setServiceCategoryCacheList(List<ServiceCategory> serviceCategoryCacheList) {
        this.serviceCategoryCacheList = serviceCategoryCacheList;
        if(serviceCategoryCacheList != null){
            serviceCategoryMap = new HashMap<>();
            for (ServiceCategory serviceCategory : serviceCategoryCacheList) {
                serviceCategoryMap.put(serviceCategory.getObjectId(), serviceCategory);
            }
        }else{
            serviceCategoryMap = null;
        }

    }



    public ServiceCategory findServiceCategoryById(String id){
        return serviceCategoryMap != null?serviceCategoryMap.get(id):null;
    }
}
