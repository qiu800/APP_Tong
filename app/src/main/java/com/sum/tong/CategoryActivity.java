package com.sum.tong;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.sum.tong.data.ServiceCategory;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class CategoryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    CategoryAdapter categoryAdapter;
    public static final String EXTRA_KEY_SERVICE_CATEGORY = "service_category";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        initView();
        loadData();
    }

    private void initView() {
        categoryAdapter = new CategoryAdapter();
        categoryAdapter.setAdapterItemClickListener(adapterItemClickListener);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(categoryAdapter);

    }

    AdapterItemClickListener<ServiceCategory> adapterItemClickListener = new AdapterItemClickListener<ServiceCategory>() {
        @Override
        public void onItemClickListener(ServiceCategory data, int position, View view) {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_KEY_SERVICE_CATEGORY, data);
            setResult(RESULT_OK, intent);
            finish();
        }
    };


    private void loadData(){
        if(App.getInstance().getCache().getServiceCategoryCacheList() != null){
            categoryAdapter.setServiceCategoryList(App.getInstance().getCache().getServiceCategoryCacheList());
        }else{
            refresh();
        }
    }

    private void refresh() {
        BmobQuery<ServiceCategory> serviceCategoryQuery = new BmobQuery<>();
        serviceCategoryQuery.addWhereEqualTo("type", 2);
        serviceCategoryQuery.addWhereEqualTo("parentId", "0");
        serviceCategoryQuery.findObjects(new FindListener<ServiceCategory>() {
            @Override
            public void done(List<ServiceCategory> list, BmobException e) {
                if(e == null){
                    App.getInstance().getCache().setServiceCategoryCacheList(list);
                    categoryAdapter.setServiceCategoryList(list);
                }else{
                    e.printStackTrace();
                    Toast.makeText(CategoryActivity.this, "加载出错", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh) {
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
