package com.sum.tong;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sum.tong.data.ServiceCategory;

import java.util.List;

/**
 * Created by Sen on 2018/3/5.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryItemView>{

    private List<ServiceCategory> serviceCategoryList;
    private AdapterItemClickListener<ServiceCategory> adapterItemClickListener;


    @Override
    public CategoryItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryItemView(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_text, parent, false));
    }

    @Override
    public void onBindViewHolder(final CategoryItemView holder, int position) {
        final ServiceCategory serviceCategory = serviceCategoryList.get(position);
        holder.textView.setText(serviceCategory.getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapterItemClickListener != null){
                    adapterItemClickListener.onItemClickListener(serviceCategory, holder.getAdapterPosition(), v);
                }
            }
        });
    }

    public void setAdapterItemClickListener(AdapterItemClickListener<ServiceCategory> adapterItemClickListener) {
        this.adapterItemClickListener = adapterItemClickListener;
    }

    @Override
    public int getItemCount() {
        return serviceCategoryList == null ? 0 :serviceCategoryList.size();
    }

    public void setServiceCategoryList(List<ServiceCategory> serviceCategoryList){
        this.serviceCategoryList = serviceCategoryList;
        notifyDataSetChanged();
    }

    static class CategoryItemView extends RecyclerView.ViewHolder{

        private TextView textView;

        public CategoryItemView(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.category_tv);
        }
    }

}
