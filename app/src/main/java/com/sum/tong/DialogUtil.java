package com.sum.tong;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

/**
 * Created by Sen on 2018/3/6.
 */

public class DialogUtil {

    public static Dialog showDialog(Context context, DialogInterface.OnClickListener onClickListener){
        return new AlertDialog.Builder(context)
                .setTitle("确认删除")
                .setMessage("确认删除该照片？")
                .setNegativeButton("取消", null)
                .setPositiveButton("确定", onClickListener)
                .show();
    }

    public static Dialog showEditDialog(Context context, EditDialogInterface.OnClickListener onClickListener){

        View view = View.inflate(context, R.layout.dialog_edit, null);
        EditText editText = (EditText)view.findViewById(R.id.edit_text);

        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setTitle("完善地址信息")
                .setView(view)
                .setNegativeButton("取消", null)
                .setPositiveButton("确定", new EditDialogInterface(editText, onClickListener))
                .show();
        return alertDialog;


    }

    public static class EditDialogInterface implements DialogInterface.OnClickListener{
        EditText editText;
        OnClickListener onClickListener;
        public EditDialogInterface(EditText editText, OnClickListener onClickListener){
            this.editText = editText;
            this.onClickListener = onClickListener;
        }

        @Override
        public void onClick(DialogInterface dialog, int which) {
            if(onClickListener != null){
                onClickListener.onClick(editText == null ? null:editText.getText().toString());
            }
        }

        public interface OnClickListener{
            void onClick(String editText);
        }
    }



}
