package com.sum.tong;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.sum.tong.data.Location;
import com.sum.tong.data.Photo;
import com.sum.tong.data.ServiceCategory;
import com.sum.tong.data.Store;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UploadBatchListener;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

public class EditStoreActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_CHOOSE_CATEGORY = 1001;
    private static final int REQUEST_CODE_CHOOSE_ADDRESS = 1002;
    private static final int REQUEST_CODE_CHOOSE = 1003;
    private static final String EXTRA_KEY_STORE = "store";
    TextInputLayout titleInputLayout;
    TextInputLayout subtitleInputLayout;
    TextInputLayout phoneInputLayout;
    TextInputLayout categoryInputLayout;
    TextInputLayout addressInputLayout;

    TextInputEditText titleInputEdit;
    TextInputEditText subtitleInputEdit;
    TextInputEditText phoneInputEdit;
    TextInputEditText categoryInputEdit;
    TextInputEditText addressInputEdit;

    Button send;

    RecyclerView recyclerView;
    PhotoAdapter photoAdapter;

    ServiceCategory serviceCategory;
    Location location;
    Store store;

    public static Intent getEditStoreIntent(Context context, Store store){
        return new Intent(context, EditStoreActivity.class).putExtra(EXTRA_KEY_STORE, store);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_store);
        initView();
        parserParams();
        checkPermission();
    }

    private void parserParams() {
        if(getIntent().hasExtra(EXTRA_KEY_STORE)){
            store = (Store) getIntent().getSerializableExtra(EXTRA_KEY_STORE);
            titleInputEdit.setText(store.getName());
            subtitleInputEdit.setText(store.getSubtitle());
            phoneInputEdit.setText(store.getPhone());

            serviceCategory = App.getInstance().getCache().findServiceCategoryById(store.getCategory());
            if(serviceCategory == null){
                serviceCategory = new ServiceCategory();
                serviceCategory.setObjectId(store.getCategory());
                serviceCategory.setTitle("获取失败");
            }
            categoryInputEdit.setText(serviceCategory.getTitle());

            location = new Location(store.getAddress(), store.getLatitude(), store.getLongitude());
            if(!TextUtils.isEmpty(location.getAddress())){
                addressInputEdit.setText(location.getAddress());
            }

            recyclerView.setVisibility(View.GONE);
        }
    }

    private void checkPermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0);
        }
    }

    private void initView() {
        titleInputLayout = (TextInputLayout) findViewById(R.id.title_text_input_layout);
        subtitleInputLayout = (TextInputLayout) findViewById(R.id.subtitle_text_input_layout);
        phoneInputLayout = (TextInputLayout) findViewById(R.id.phone_text_input_layout);
        categoryInputLayout = (TextInputLayout) findViewById(R.id.category_text_input_layout);
        addressInputLayout = (TextInputLayout) findViewById(R.id.address_text_input_layout);

        titleInputEdit = (TextInputEditText) findViewById(R.id.title_text_input_edit);
        subtitleInputEdit = (TextInputEditText) findViewById(R.id.subtitle_text_input_edit);
        phoneInputEdit = (TextInputEditText) findViewById(R.id.phone_text_input_edit);
        categoryInputEdit = (TextInputEditText) findViewById(R.id.category_text_input_edit);
        addressInputEdit = (TextInputEditText) findViewById(R.id.address_text_input_edit);

        send = (Button) findViewById(R.id.send);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        categoryInputLayout.setOnClickListener(listener);
        categoryInputEdit.setOnClickListener(listener);
        addressInputLayout.setOnClickListener(listener);
        addressInputEdit.setOnClickListener(listener);
        send.setOnClickListener(listener);

        photoAdapter = new PhotoAdapter();
        photoAdapter.setAdapterItemClickListener(photoAdapterItemClickListener);
        recyclerView.setAdapter(photoAdapter);
    }

    AdapterItemClickListener<Photo> photoAdapterItemClickListener = new AdapterItemClickListener<Photo>() {
        @Override
        public void onItemClickListener(Photo data, final int position, View view) {
            if(data == null){
                Matisse.from(EditStoreActivity.this)
                        .choose(MimeType.allOf())
                        .countable(true)
                        .maxSelectable(10 - photoAdapter.getItemCount())
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                        .thumbnailScale(0.85f)
                        .imageEngine(new GlideEngine())
                        .forResult(REQUEST_CODE_CHOOSE);
            }else{
                DialogUtil.showDialog(view.getContext(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        photoAdapter.delete(position);
                    }
                });
            }
        }
    };

    View.OnClickListener listener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.send:
                    doSend();
                    break;
                case R.id.category_text_input_edit:
                case R.id.category_text_input_layout:
                    chooseCategory();
                    break;
                case R.id.address_text_input_edit:
                case R.id.address_text_input_layout:
                    chooseAddress();
                    break;
            }
        }
    };

    /**
     * 选择地址
     */
    private void chooseAddress() {
        startActivityForResult(new Intent(this, MapActivity.class), REQUEST_CODE_CHOOSE_ADDRESS);
    }

    /**
     * 选择分类
     */
    private void chooseCategory() {
        startActivityForResult(new Intent(this, CategoryActivity.class), REQUEST_CODE_CHOOSE_CATEGORY);
    }




    /**
     * 发送
     */
    private void doSend() {
        Store store = getStore();
        if(store == null)
            return;
        List<Photo> photos = photoAdapter.getData();
        if(photos != null && photos.size() > 0){
            List<String> photoFilePaths = new ArrayList<>();
            for (Photo photo : photos) {
                photoFilePaths.add(photo.getPath());
            }

            compressImage(store, photoFilePaths);

        }else{
            save(store);
        }
    }

    private void compressImage(final Store store, List<String> photoFilePaths) {
        final int totalSize = photoFilePaths.size();
        Luban.with(this)
                .load(photoFilePaths)
                .ignoreBy(100)
                .setTargetDir(getExternalFilesDir(Environment.DIRECTORY_PICTURES).getPath())
                .setCompressListener(new OnCompressListener() { //设置回调
                    int successCount = 0;
                    int compressCount = 0;
                    String[] photoPathArray = new String[totalSize];
                    @Override
                    public void onStart() {
                        compressCount++;
                        showProgress(String.format("正在压缩第%s张图片...", compressCount));
                    }

                    @Override
                    public void onSuccess(File file) {
                        Log.v("EditStoreActivity", String.format("压缩图片路径:%s", file.getPath()));
                        photoPathArray[successCount] = file.getPath();
                        successCount++;
                        if(successCount == totalSize){
                            hideProgress();
                            upImageAndCommit(store, photoPathArray);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideProgress();
                        Toast.makeText(EditStoreActivity.this, "压缩图片错误", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }).launch();    //启动压缩
    }

    private void save(Store store){
        showProgress("正在保存商家信息...");
        store.save(new SaveListener<String>() {
            @Override
            public void done(String s, BmobException e) {
                Toast.makeText(EditStoreActivity.this, "保存成功！", Toast.LENGTH_LONG).show();
                hideProgress();
                reset();
            }
        });
    }

    private void reset(){
        if(titleInputLayout == null)
            return;
        titleInputLayout.setErrorEnabled(false);
        titleInputEdit.setText(null);
        subtitleInputLayout.setErrorEnabled(false);
        subtitleInputEdit.setText(null);
        phoneInputLayout.setErrorEnabled(false);
        phoneInputEdit.setText(null);
        categoryInputLayout.setErrorEnabled(false);
        categoryInputEdit.setText("选择分类");
        addressInputLayout.setErrorEnabled(false);
        addressInputEdit.setText("商家位置");
        photoAdapter.clear();
        store = null;

    }

    private boolean showError(TextInputLayout textInputLayout, String errorMessage, boolean isShow){
        if(isShow){
            textInputLayout.setError(errorMessage);
        }else{
            textInputLayout.setErrorEnabled(false);
        }

        return !isShow;
    }

    private void upImageAndCommit(final Store store, @NonNull String[] photoFilePaths){
        BmobFile.uploadBatch(photoFilePaths, new UploadBatchListener() {
            @Override
            public void onSuccess(List<BmobFile> files,List<String> urls) {
                hideProgress();
                store.setPic_urls(urls);
                save(store);
            }

            @Override
            public void onError(int statuscode, String errormsg) {
                Toast.makeText(EditStoreActivity.this, "错误码"+statuscode +",错误描述："+errormsg, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onProgress(int curIndex, int curPercent, int total,int totalPercent) {
                showProgress(String.format("正在上传%s/%s 进度:%s", curIndex, total, totalPercent));
            }
        });

    }

    ProgressDialog progressDialog;
    private void showProgress(String message){
        if(progressDialog == null){
            progressDialog = new ProgressDialog(this);
        }
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void hideProgress(){
        if(progressDialog != null)
            progressDialog.dismiss();
    }

    private Store getStore(){
        String name = titleInputEdit.getText().toString();
        String subtitle = subtitleInputEdit.getText().toString();
        String phone = phoneInputEdit.getText().toString();
        String category = categoryInputEdit.getText().toString();
        String address = addressInputEdit.getText().toString();

        boolean result = showError(titleInputLayout, "标题不能为空", TextUtils.isEmpty(name));

        result = showError(subtitleInputLayout, "简介不能为空", TextUtils.isEmpty(subtitle)) && result;
        result = showError(phoneInputLayout, "联系电话不能为空", TextUtils.isEmpty(phone)) && result;
        result = showError(categoryInputLayout, "分类不能为空", "选择分类".equals(category)) && result;
        result = showError(addressInputLayout, "商家位置不能为空", "商家位置".equals(address)) && result;
        if(!result){
            return null;
        }
        if(store == null){
            store = new Store();
        }
        store.setName(name);
        store.setSubtitle(subtitle);
        store.setPhone(phone);
        store.setAddress(address);
        store.setCategory(serviceCategory.getObjectId());
        store.setLatitude(location.getLatitude());
        store.setLongitude(location.getLongitude());
        return store;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != RESULT_OK)
            return;
        if(requestCode == REQUEST_CODE_CHOOSE_CATEGORY){
            serviceCategory = (ServiceCategory) data.getSerializableExtra(CategoryActivity.EXTRA_KEY_SERVICE_CATEGORY);
            categoryInputEdit.setText(serviceCategory.getTitle());
        }else if(requestCode == REQUEST_CODE_CHOOSE_ADDRESS){
            location = (Location) data.getSerializableExtra(MapActivity.EXTRA_KEY_LOCATION);
            addressInputEdit.setText(location.getAddress());
        }else if(requestCode == REQUEST_CODE_CHOOSE){
            photoAdapter.addDataByUri(this, Matisse.obtainResult(data));
            photoAdapter.notifyDataSetChanged();
        }
    }


}
