package com.sum.tong;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Toast;

import com.sum.tong.data.Store;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class MainActivity extends Activity {

    private SearchView searchView;
    private RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    FloatingActionButton floatingActionButton;
    StoreAdapter storeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        doSearch(null);

    }

    private void initView() {
        searchView = (SearchView) findViewById(R.id.search_view);
        floatingActionButton = findViewById(R.id.add_store);
        swipeRefreshLayout = findViewById(R.id.refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        storeAdapter = new StoreAdapter();
        storeAdapter.setAdapterItemClickListener(adapterItemClickListener);
        recyclerView.setAdapter(storeAdapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                doSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, EditStoreActivity.class));
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doSearch(null);
            }
        });


    }

    AdapterItemClickListener<Store> adapterItemClickListener = new AdapterItemClickListener<Store>() {
        @Override
        public void onItemClickListener(Store data, int position, View view) {
            startActivity(EditStoreActivity.getEditStoreIntent(MainActivity.this, data));
        }
    };

    private void doSearch(String query) {
        BmobQuery<Store> storeBmobQuery = new BmobQuery<>();
        if(query != null && !query.isEmpty())
            storeBmobQuery.addWhereEqualTo("name", query);
        storeBmobQuery.setLimit(20);
        storeBmobQuery.order("-updatedAt");
        storeBmobQuery.findObjects(new FindListener<Store>() {
            @Override
            public void done(List<Store> list, BmobException e) {
                if(e == null){
                    storeAdapter.setData(list);
                }else{
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "加载出错", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


}
