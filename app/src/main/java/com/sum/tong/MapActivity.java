package com.sum.tong;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.widget.Toast;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.CameraPosition;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.maps2d.model.MyLocationStyle;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.sum.tong.data.Location;

import java.util.List;

public class MapActivity extends AppCompatActivity implements PoiSearch.OnPoiSearchListener{

    public static final String EXTRA_KEY_LOCATION = "location";
    private MapView mapView;
    private RecyclerView recyclerView;
    private SearchView searchView;
    private TipAdapter tipAdapter;
    private Location chooseLocation;
    private Marker centerMarker;
    PoiSearch poiSearch;
    AMap aMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        initView(savedInstanceState);
        checkPermission();


    }

    private void checkPermission() {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission_group.LOCATION) == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission_group.LOCATION},
                    0);
        }
    }

    private void initView(Bundle savedInstanceState) {
        tipAdapter = new TipAdapter();
        searchView = (SearchView) findViewById(R.id.search_view);
        mapView = (MapView) findViewById(R.id.map_view);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(tipAdapter);

        mapView.onCreate(savedInstanceState);
        if (aMap == null) {
            aMap = mapView.getMap();
        }

        initMapView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                doSearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // todo 搜索提示
                return false;
            }
        });
    }

    private void initMapView() {

        MyLocationStyle myLocationStyle = new MyLocationStyle();
        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATE);
        myLocationStyle.showMyLocation(true);
        aMap.setMyLocationStyle(myLocationStyle);
        aMap.setMyLocationEnabled(true);
        aMap.moveCamera(CameraUpdateFactory.zoomBy(18));
        aMap.setOnCameraChangeListener(new AMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if(centerMarker != null){
                    centerMarker.remove();
                }
                centerMarker = aMap.addMarker(new MarkerOptions()
                        .position(cameraPosition.target)
                        .title("自定义位置")
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.pin)));
            }

            @Override
            public void onCameraChangeFinish(CameraPosition cameraPosition) {

            }
        });
        aMap.setOnMarkerClickListener(new AMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Location location = new Location();
                location.setAddress(marker.getTitle());
                location.setLatitude(marker.getPosition().latitude);
                location.setLongitude(marker.getPosition().longitude);
                chooseLocation = location;
                DialogUtil.showEditDialog(MapActivity.this, new DialogUtil.EditDialogInterface.OnClickListener() {
                    @Override
                    public void onClick(String editText) {
                        if(editText != null){
                            chooseLocation.setAddress(editText);
                        }
                        Intent intent = new Intent();
                        intent.putExtra(EXTRA_KEY_LOCATION, chooseLocation);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
                return false;
            }
        });
    }


    private void doSearch(String key){
        PoiSearch.Query query = new PoiSearch.Query(key, "", "保亭");
        query.setPageSize(10);
        if(poiSearch == null){
            poiSearch = new PoiSearch(this, query);
            poiSearch.setOnPoiSearchListener(this);
        }else{
            poiSearch.setQuery(query);
        }
        poiSearch.searchPOIAsyn();
    }

    @Override
    public void onPoiSearched(PoiResult poiResult, int i) {
        if(1000 == i){
            List<PoiItem> poiItems = poiResult.getPois();
            if(poiItems == null || poiItems.size() == 0){
                Toast.makeText(this, "搜索结果为空", Toast.LENGTH_LONG).show();
            }else{
                Marker marker = null;
                for (int j = poiItems.size() - 1; j >= 0; j--) {
                    marker = drawMarker(poiItems.get(j));
                    marker.showInfoWindow();
                }
                moveFirstMarker(marker);

            }
        }else{
            Toast.makeText(this, "搜索失败， 错误码:" + i, Toast.LENGTH_LONG).show();
        }
    }

    private void moveFirstMarker(Marker marker) {
        aMap.moveCamera(CameraUpdateFactory.zoomBy(18));
        aMap.moveCamera(CameraUpdateFactory.changeLatLng(marker.getPosition()));
    }


    @Override
    public void onPoiItemSearched(PoiItem poiItem, int i) {

    }

    private Marker drawMarker(PoiItem poiItem){
        LatLng latLng = new LatLng(poiItem.getLatLonPoint().getLatitude(), poiItem.getLatLonPoint().getLongitude());
        return aMap.addMarker(new MarkerOptions().position(latLng).title(poiItem.getTitle()));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.v("test", "onRequestPermissionsResult");
    }
}
