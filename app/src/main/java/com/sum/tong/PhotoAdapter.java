package com.sum.tong;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sum.tong.data.Photo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sen on 2018/2/28.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoItemView>{

    private List<Photo> dataList;
    private AdapterItemClickListener<Photo> adapterItemClickListener;

    @Override
    public PhotoItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoItemView(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_image_view, parent, false));
    }


    @Override
    public void onBindViewHolder(final PhotoAdapter.PhotoItemView holder, int position) {
        try{
            final Photo photo = dataList.get(position);
            Glide.with(holder.itemView.getContext()).load(photo.getPath()).into(holder.imageView);
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapterItemClickListener != null) {
                        adapterItemClickListener.onItemClickListener(photo, holder.getAdapterPosition(), v);
                    }
                }
            });
        }catch (Exception e){
            holder.imageView.setImageResource(R.drawable.ic_add_24dp);
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapterItemClickListener != null) {
                        adapterItemClickListener.onItemClickListener(null, holder.getAdapterPosition(), v);
                    }
                }
            });
        }
    }

    public void delete(int position){
        dataList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        int size = dataList != null ? dataList.size():0;
        return Math.min(size + 1, 9);
    }

    public List<Photo> getData() {
        return dataList;
    }

    public void addData(List<Photo> photoList){
        if(dataList == null){
            dataList = new ArrayList<>();
        }

        dataList.addAll(photoList);
    }

    public void addDataByUri(Context context, List<Uri> uriList){
        List<Photo> photos = new ArrayList<>();
        for(Uri uri : uriList){
            Photo photo = new Photo();
            String path = PictureUtils.getPathFromUri(context, uri);
            photo.setPath(path);
            photos.add(photo);
        }

        if(photos.size() > 0){
            addData(photos);
        }
    }

    public void setAdapterItemClickListener(AdapterItemClickListener<Photo> adapterItemClickListener) {
        this.adapterItemClickListener = adapterItemClickListener;
    }

    public void clear() {
        dataList = null;
        notifyDataSetChanged();
    }

    public class PhotoItemView extends RecyclerView.ViewHolder{

        private ImageView imageView;

        public PhotoItemView(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image_view);
        }
    }



}
