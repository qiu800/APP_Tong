package com.sum.tong;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

/**
 * Created by Sen on 2018/3/6.
 */

public class PictureUtils {

    public static String getPathFromUri(Context context, Uri uri) {
        String outPath = "";
        Cursor cursor = context.getContentResolver()
                .query(uri, null, null, null, null);
        if (cursor == null) {
            return uri.getPath();
        } else {
            if (uri.toString().contains("content://com.android.providers.media.documents/document/image")) { // htc 某些手机
                // 获取图片地址
                String _id = null;
                String uri_decode = uri.decode(uri.toString());
                int id_index = uri_decode.lastIndexOf(":");
                _id = uri_decode.substring(id_index + 1);
                Cursor mCursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, " _id = " + _id,
                        null, null);
                mCursor.moveToFirst();
                int column_index = mCursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                outPath = mCursor.getString(column_index);
                if (!mCursor.isClosed()) {
                    mCursor.close();
                }
                if (!cursor.isClosed()) {
                    cursor.close();
                }
                return outPath;
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (DocumentsContract.isDocumentUri(context, uri)) {
                        String docId = DocumentsContract.getDocumentId(uri);
                        if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
                            //Log.d(TAG, uri.toString());
                            String id = docId.split(":")[1];
                            String selection = MediaStore.Images.Media._ID + "=" + id;
                            outPath = getImagePath(context, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection);
                        } else if ("com.android.providers.downloads.documents".equals(uri.getAuthority())) {
                            //Log.d(TAG, uri.toString());
                            Uri contentUri = ContentUris.withAppendedId(
                                    Uri.parse("content://downloads/public_downloads"),
                                    Long.valueOf(docId));
                            outPath = getImagePath(context, contentUri, null);
                        }
                        return outPath;
                    }
                }
                if ("content".equalsIgnoreCase(uri.getScheme())) {
                    String auth = uri.getAuthority();
                    if (auth.equals("media")) {
                        outPath = getImagePath(context, uri, null);
                    } else if (auth.equals("com.pcb.mall.fileprovider")) {
                        //参看file_paths_public配置
                        outPath = Environment.getExternalStorageDirectory() + "/Pictures/" + uri.getLastPathSegment();
                    }
                    return outPath;
                }
            }
            return outPath;
        }

    }

    private static String getImagePath(Context context, Uri uri, String selection) {
        String path = null;
        Cursor cursor = context.getContentResolver().query(uri, null, selection, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }

}
