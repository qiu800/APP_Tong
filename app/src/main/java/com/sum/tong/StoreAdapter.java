package com.sum.tong;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sum.tong.data.Store;

import java.util.List;

/**
 * Created by Sen on 2018/3/22.
 */

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.StoreItemView> {

    private List<Store> storeList;
    private AdapterItemClickListener<Store> adapterItemClickListener;


    @Override
    public StoreItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StoreItemView(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_text, parent, false));
    }

    @Override
    public void onBindViewHolder(final StoreItemView holder, int position) {
        final Store store = storeList.get(position);
        holder.textView.setText(store.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapterItemClickListener != null) {
                    adapterItemClickListener.onItemClickListener(store, holder.getAdapterPosition(), v);
                }
            }
        });
    }

    public void setAdapterItemClickListener(AdapterItemClickListener<Store> adapterItemClickListener) {
        this.adapterItemClickListener = adapterItemClickListener;
    }

    @Override
    public int getItemCount() {
        return storeList == null ? 0 : storeList.size();
    }

    public void setData(List<Store> storeList) {
        this.storeList = storeList;
        notifyDataSetChanged();
    }

    public void addData(List<Store> storeList) {
        if(this.storeList == null){
            this.storeList = storeList;
        }else{
            this.storeList.addAll(storeList);
        }
    }

    static class StoreItemView extends RecyclerView.ViewHolder {

        private TextView textView;

        public StoreItemView(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.category_tv);
        }
    }
}