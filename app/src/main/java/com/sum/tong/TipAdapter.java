package com.sum.tong;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amap.api.services.help.Tip;

import java.util.List;

/**
 * Created by Sen on 2018/3/5.
 */

public class TipAdapter extends RecyclerView.Adapter<TipAdapter.TipItemView> {

    private List<Tip> dataList;
    private AdapterItemClickListener<Tip> adapterItemClickListener;

    @Override
    public TipItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TipItemView(View.inflate(parent.getContext(), R.layout.item_photo_image_view, null));
    }

    @Override
    public void onBindViewHolder(final TipItemView holder, int position) {
        final Tip tip = dataList.get(position);
        holder.textView.setText(tip.getAddress());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapterItemClickListener != null) {
                    adapterItemClickListener.onItemClickListener(tip, holder.getAdapterPosition(), v);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }

    public void setData(List<Tip> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    public List<Tip> getData() {
        return dataList;
    }

    public void setAdapterItemClickListener(AdapterItemClickListener<Tip> adapterItemClickListener) {
        this.adapterItemClickListener = adapterItemClickListener;
    }

    public class TipItemView extends RecyclerView.ViewHolder {

        private TextView textView;

        public TipItemView(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tip_tv);
        }
    }


}
