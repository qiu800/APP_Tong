package com.sum.tong.data;

import java.io.Serializable;

/**
 * Created by Sen on 2018/2/28.
 */

public class Location implements Serializable{

    private String address;
    private double latitude;
    private double longitude;

    public Location(){}

    public Location(String address, double latitude, double longitude){
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
