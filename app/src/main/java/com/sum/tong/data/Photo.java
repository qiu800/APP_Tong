package com.sum.tong.data;

/**
 * Created by Sen on 2018/2/28.
 */

public class Photo {

    private String path;
    private String url;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
