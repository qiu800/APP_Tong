package com.sum.tong.data;

import cn.bmob.v3.BmobObject;

/**
 * Created by Sen on 2018/2/28.
 */

public class ServiceCategory extends BmobObject{

    private double type;
    private String title;
    private String parentId;

    public double getType() {
        return type;
    }

    public void setType(double type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
